from PIL import Image


class Screen:
    def __init__(self, screen_params):
        self.img = Image.new('RGB', (screen_params["width"], screen_params["height"]), screen_params["bg_color"])
        self.param = screen_params
        if "fg_color" in screen_params:
            self.set_fg_default_color()

    bg_default = "black"
    fg_default = "white"

    def set_fg_default_color(self):
        self.fg_default = self.param["fg_color"]


class Palette:
    def __init__(self, colours):
        self.dict_of_colours = colours

    def get_colour(self, color):
        if color in self.dict_of_colours:
            return self.dict_of_colours[color]
        else:
            if color[0] == '(' and color[-1] == ')':
                return 'rgb'+color
            return color
